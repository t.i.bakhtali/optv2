﻿using System;
using System.Collections.Generic;

namespace PickUpProblem
{
    [Serializable()]
    class WorkDay
    {
        public Route[] Routes;
        public float Duration { get; private set; }
        public WorkDay(int Nroutes)
        {
            Routes = new Route[Nroutes];
            for (int i = 0; i < Nroutes; i++)
            {
                Routes[i] = new Route();
            }
        }

        public void UpdateDuration(float newDur)
        {
            Duration = newDur;
        }
    }

    [Serializable()]
    class Truck
    {
        // Pickup frequency defined as bits
        const int f1_1 = 1;  // 00001 | Monday
        const int f1_2 = 2;  // 00010 | Tuesday
        const int f1_3 = 4;  // 00100 | Wednesday
        const int f1_4 = 8;  // 01000 | Thursday
        const int f1_5 = 16; // 10000 | Friday
        const int f2_1 = 9;  // 01001 | Monday-Thursday
        const int f2_2 = 18; // 10010 | Tuesday-Friday
        const int f3 = 21;   // 10101 | Three times a week 
        const int f4_1 = 30; // 11110 | Four times a week
        const int f4_2 = 29; // 11101 | etc.
        const int f4_3 = 27; // 11011
        const int f4_4 = 23; // 10111
        const int f4_5 = 15; // 01111
        public Dictionary<BitDay, WorkDay> WorkDays { get; private set; } = new Dictionary<BitDay, WorkDay>();
        BitDay[] bitDays = new BitDay[] { BitDay.Monday, BitDay.Tuesday, BitDay.Wednesday, BitDay.Thursday, BitDay.Friday };

        public Truck()
        {
            foreach (BitDay day in bitDays)
            {
                WorkDays.Add(day, new WorkDay(2));
            }
        }

        public WorkDay GetWorkDay(BitDay day)
        {
            return WorkDays[day];
        }

        public static bool CheckFrequency(int frequency, int visits)
        {
            switch (frequency)
            {
                case 1:
                    {
                        if ((~visits & f1_1) == 0) return true;
                        if ((~visits & f1_2) == 0) return true;
                        if ((~visits & f1_3) == 0) return true;
                        if ((~visits & f1_4) == 0) return true;
                        if ((~visits & f1_5) == 0) return true;
                    }
                    break;
                case 2:
                    {
                        if ((~visits & f2_1) == 0) return true;
                        if ((~visits & f2_2) == 0) return true;
                    }
                    break;
                case 3:
                    {
                        if ((~visits & f3) == 0) return true;
                    }
                    break;
                case 4:
                    {
                        if ((~visits & f4_1) == 0) return true;
                        if ((~visits & f4_2) == 0) return true;
                        if ((~visits & f4_3) == 0) return true;
                        if ((~visits & f4_4) == 0) return true;
                        if ((~visits & f4_5) == 0) return true;
                    }
                    break;
            }
            return false;
        }

        public BitDay GetRandomDay()
        {
            return bitDays[Program.Random.Next(bitDays.Length)];
        }
    }
}
