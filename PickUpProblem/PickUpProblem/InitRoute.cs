﻿using System;
using System.Collections.Generic;

namespace PickUpProblem
{
    class InitRoute
    {
        List<BitDay>[] F2 = new List<BitDay>[]
        {
            new List<BitDay>() { BitDay.Monday, BitDay.Thursday, },
            new List<BitDay>() { BitDay.Tuesday, BitDay.Friday }
        };
        List<BitDay> F3 = new List<BitDay>() { BitDay.Monday, BitDay.Wednesday, BitDay.Friday };
        List<BitDay>[] F4 = new List<BitDay>[]
        {
            new List<BitDay>() { BitDay.Monday, BitDay.Tuesday, BitDay.Wednesday, BitDay.Thursday, /*          */},
            new List<BitDay>() { BitDay.Monday, BitDay.Tuesday, BitDay.Wednesday, /*            */ BitDay.Friday },
            new List<BitDay>() { BitDay.Monday, BitDay.Tuesday, /*             */ BitDay.Thursday, BitDay.Friday },
            new List<BitDay>() { BitDay.Monday, /*           */ BitDay.Wednesday, BitDay.Thursday, BitDay.Friday },
            new List<BitDay>() { /*          */ BitDay.Tuesday, BitDay.Wednesday, BitDay.Thursday, BitDay.Friday }
        };

        public void Apply(Truck[] trucks, ref float score)
        {
            if (!Data.SeperateHighFreq) return;

            foreach (int orderID in Data.HighFreq)
            {
                Order parent = Program.Database.GetOrder(orderID);
                List<BitDay> days = new List<BitDay>(GetRandomFrequency(parent.Frequency));

                BitDay day = PullRandomDay(ref days);
                Connect(trucks, parent, day, ref score);
                for (int i = 0; i < parent.Children.Count; i++)
                {
                    Order child = Program.Database.GetOrder(parent.Children[i]);
                    day = PullRandomDay(ref days);
                    Connect(trucks, child, day, ref score);
                }
                score -= parent.EmptyingTime * 3;
            }
        }

        private void Connect(Truck[] trucks, Order targetToAdd, BitDay day, ref float score)
        {
            Truck truck = trucks[Program.Random.Next(trucks.Length)];
            WorkDay workDay = truck.GetWorkDay(day);
            Route route = workDay.Routes[Program.Random.Next(workDay.Routes.Length)];
            bool infront = Program.Random.Next(2) == 1;
            Order routeOrder = route.Peek(out int index);

            // no need to remove from notvisited because they are not put there initially

            // update duration
            float newDuration = workDay.Duration;
            newDuration += targetToAdd.EmptyingTime;

            if (route.Count == 0)
            {
                newDuration += 30;
            }

            if (routeOrder != null)
            {
                if (infront)
                {
                    int previousOrderMatrixID = Operator.GetPreviousMatrixID(routeOrder);
                    newDuration -= Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, routeOrder.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, targetToAdd.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, routeOrder.MatrixID);
                }
                else
                {
                    int nextOrderMatrixID = Operator.GetNextMatrixID(routeOrder);
                    newDuration -= Program.Matrix.GetDriveTimeInMin(routeOrder.MatrixID, nextOrderMatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(routeOrder.MatrixID, targetToAdd.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, nextOrderMatrixID);
                }

                Order previous = routeOrder.PreviousOrder;
                Order next = routeOrder.NextOrder;

                if (infront)
                {
                    targetToAdd.Link(ref previous, ref routeOrder);
                }
                else
                {
                    targetToAdd.Link(ref routeOrder, ref next);
                }
            }
            else
            {
                newDuration += Program.Matrix.GetDriveTimeInMin(Program.baseMatrixID, targetToAdd.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, Program.baseMatrixID);
            }
            score += newDuration - workDay.Duration;

            targetToAdd.SetDay(day);
            workDay.UpdateDuration(newDuration);
            // update weight
            float newWeight = route.Weight + (targetToAdd.VolumeContainer * targetToAdd.NContainers);
            route.UpdateWeight(newWeight);
            // add to route
            route.Push(targetToAdd.ID);
        }

        private BitDay PullRandomDay(ref List<BitDay> bitDays)
        {
            int index = Program.Random.Next(bitDays.Count);
            BitDay bitDay = bitDays[index];
            bitDays.RemoveAt(index);
            return bitDay;
        }

        private List<BitDay> GetRandomFrequency(int frequency)
        {
            switch (frequency)
            {
                case 2:
                    {
                        return F2[Program.Random.Next(F2.Length)];
                    }
                case 3:
                    {
                        return F3;
                    }
                case 4:
                    {
                        return F4[Program.Random.Next(F4.Length)];
                    }
            }
            return null;
        }
    }
}
