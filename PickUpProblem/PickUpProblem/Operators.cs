﻿using System;
using System.Collections.Generic;

namespace PickUpProblem
{
    abstract class Operator
    {
        public abstract bool Try(Truck[] truck, out float score);
        public abstract void Apply();

        public static int GetPreviousMatrixID(Order order)
        {
            int previousID = Program.baseMatrixID;
            if (order.PreviousOrder != null)
            {
                previousID = order.PreviousOrder.MatrixID;
            }
            return previousID;
        }
        public static int GetNextMatrixID(Order order)
        {
            int nextID = Program.baseMatrixID;
            if (order.NextOrder != null)
            {
                nextID = order.NextOrder.MatrixID;
            }
            return nextID;
        }
    }

    class AddOrder : Operator
    {
        Order targetToAdd;
        Order routeOrder;
        Route route;
        BitDay day;
        WorkDay workDay;
        bool infront;
        int randomIndexNotVisited;
        float newDuration;
        float newWeight;
        Truck targetTruck;

        public override bool Try(Truck[] truck, out float score)
        {
            // init
            score = 0;
            if (Program.NotVisited.Count == 0)
            {
                return false;
            }
            // random calls
            targetTruck = truck[Program.Random.Next(truck.Length)];
            infront = Program.Random.Next(2) == 1;

            targetToAdd = Program.NotVisited.Peek(out randomIndexNotVisited);
            day = targetTruck.GetRandomDay();
            while ((targetToAdd.Frequency == 2 && day == BitDay.Wednesday) || (targetToAdd.Frequency == 3 && (day == BitDay.Tuesday || day == BitDay.Thursday)))
                day = targetTruck.GetRandomDay();
            workDay = targetTruck.GetWorkDay(day);
            route = workDay.Routes[Program.Random.Next(2)];
            routeOrder = route.Peek(out int index);

            // current route
            newDuration = workDay.Duration;
            if (route.Count == 0)
            {
                newDuration += 30; //add 30 minutes for emptying
            }

            int previousOrderMatrixID = -1;
            int nextOrderMatrixID = -1;
            if (routeOrder != null)
            {
                if (infront)
                {
                    previousOrderMatrixID = GetPreviousMatrixID(routeOrder);
                }
                else
                {
                    nextOrderMatrixID = GetNextMatrixID(routeOrder);
                }
            }

            // weight increase
            newWeight = route.Weight + (targetToAdd.VolumeContainer * targetToAdd.NContainers);
            if (newWeight > Program.maxCapacity)
            {
                return false;  //not allowed to exceed the max capacity
            }

            // duration increase
            newDuration += targetToAdd.EmptyingTime;
            float traveltime = 0;

            if (routeOrder != null)
            {
                if (infront)
                {
                    traveltime += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, routeOrder.MatrixID);
                    traveltime += Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, targetToAdd.MatrixID);
                    traveltime -= Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, routeOrder.MatrixID);
                }
                else
                {
                    traveltime += Program.Matrix.GetDriveTimeInMin(routeOrder.MatrixID, targetToAdd.MatrixID);
                    traveltime += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, nextOrderMatrixID);
                    traveltime -= Program.Matrix.GetDriveTimeInMin(routeOrder.MatrixID, nextOrderMatrixID);
                }
            }
            else
            {
                traveltime += Program.Matrix.GetDriveTimeInMin(Program.baseMatrixID, targetToAdd.MatrixID);
                traveltime += Program.Matrix.GetDriveTimeInMin(targetToAdd.MatrixID, Program.baseMatrixID);
            }
            newDuration += traveltime;
            if (newDuration > Program.maxWorkMinutes)
            {
                return false; // not allowed to exceed the time of a day
            }

            // decline penalty
            int frequency = targetToAdd.Frequency;
            if (frequency == 1)
            {
                score -= targetToAdd.EmptyingTime * 3;
            }
            else
            {
                int parentID = targetToAdd.OriginalID;
                Order parent = Program.Database.GetOrder(parentID);
                int visits = 0; // set current day because target has no day assigned

                visits |= (int)parent.BitDay;
                for (int i = 0; i < parent.Children.Count; i++)
                {
                    Order child = Program.Database.GetOrder(parent.Children[i]);
                    if (child.BitDay == day || parent.BitDay == day)
                        return false;   //refuse to add multiple on the same day
                    visits |= (int)child.BitDay;
                }
                int temp = visits;
                if (Truck.CheckFrequency(frequency, temp |= (int)day))
                {
                    score -= parent.EmptyingTime * parent.Frequency * 3;
                }
                else if (visits != 0 && frequency == 2)
                {
                    return false; //refuse to add on wrong day
                }
            }
            score += newDuration - workDay.Duration;
            return true;
        }

        public override void Apply()
        {
            Order previousRandom = null;
            Order nextRandom = null;
            if (routeOrder != null)
            {
                previousRandom = routeOrder.PreviousOrder;
                nextRandom = routeOrder.NextOrder;
            }
            if (infront)
            {
                targetToAdd.Link(ref previousRandom, ref routeOrder);
            }
            else
            {
                targetToAdd.Link(ref routeOrder, ref nextRandom);
            }
            targetToAdd.SetDay(day);
            Program.NotVisited.Remove(randomIndexNotVisited);
            route.Push(targetToAdd.ID);
            workDay.UpdateDuration(newDuration);
            route.UpdateWeight(newWeight);
        }

    }

    class RemoveOrder : Operator
    {
        WorkDay workDay = null;
        Route route = null;
        int targetIndex;

        Order targetToRemove = null;
        float newDuration;
        float newWeight;
        Truck targetTruck;
        public override bool Try(Truck[] truck, out float score)
        {
            score = 0;
            targetTruck = truck[Program.Random.Next(truck.Length)];
            workDay = targetTruck.GetWorkDay(targetTruck.GetRandomDay());
            route = workDay.Routes[Program.Random.Next(workDay.Routes.Length)];
            // orders
            targetToRemove = route.Peek(out targetIndex);
            if (targetToRemove == null)
            {
                return false;
            }
            // id
            int previousID = GetPreviousMatrixID(targetToRemove);
            int nextID = GetNextMatrixID(targetToRemove);
            // update duration
            newDuration = workDay.Duration;
            if (route.Count == 1)
            {
                newDuration -= 30; //remove 30 minutes since we will have 1 trip less to base
            }
            newDuration -= targetToRemove.EmptyingTime;
            float traveltime = 0;
            traveltime -= Program.Matrix.GetDriveTimeInMin(previousID, targetToRemove.MatrixID);
            traveltime -= Program.Matrix.GetDriveTimeInMin(targetToRemove.MatrixID, nextID);
            traveltime += Program.Matrix.GetDriveTimeInMin(previousID, nextID);
            newDuration += traveltime;

            // update weight
            newWeight = route.Weight;
            newWeight -= targetToRemove.VolumeContainer * targetToRemove.NContainers;

            // check frequency
            int frequency = targetToRemove.Frequency;
            if (frequency == 1)
            {
                score += targetToRemove.EmptyingTime * 3;
            }
            else
            {
                int parentID = targetToRemove.OriginalID;
                Order parent = Program.Database.GetOrder(parentID);
                int visits = 0;
                visits |= (int)parent.BitDay;
                for (int i = 0; i < parent.Children.Count; i++)
                {
                    Order child = Program.Database.GetOrder(parent.Children[i]);
                    visits |= (int)child.BitDay;
                }
                if (Truck.CheckFrequency(frequency, visits))
                {
                    score += parent.EmptyingTime * parent.Frequency * 3;
                }
            }
            score += newDuration - workDay.Duration;
            return true;
        }

        public override void Apply()
        {
            targetToRemove.Unlink();
            workDay.UpdateDuration(newDuration);
            route.UpdateWeight(newWeight);
            route.Remove(targetIndex);
            Program.NotVisited.Push(targetToRemove.ID);
        }
    }

    class SwapOrder : Operator
    {
        WorkDay workDay = null;
        Route route = null;

        Order targetToSwap1 = null;
        Order previous1 = null;
        Order next1 = null;
        Order targetToSwap2 = null;
        Order previous2 = null;
        Order next2 = null;
        float newDuration;
        Truck targetTruck;

        public override bool Try(Truck[] truck, out float score)
        {
            score = 0;
            targetTruck = truck[Program.Random.Next(truck.Length)];
            workDay = targetTruck.GetWorkDay(targetTruck.GetRandomDay());
            route = workDay.Routes[Program.Random.Next(workDay.Routes.Length)];

            if (route.Count <= 1)
            {
                return false;
            }

            targetToSwap1 = route.Peek(out int index1);
            targetToSwap2 = route.Peek(out int index2);
            while (index1 == index2)
                targetToSwap2 = route.Peek(out index2);

            previous1 = targetToSwap1.PreviousOrder;
            next1 = targetToSwap1.NextOrder;
            previous2 = targetToSwap2.PreviousOrder;
            next2 = targetToSwap2.NextOrder;
            int previousID1 = GetPreviousMatrixID(targetToSwap1);
            int nextID1 = GetNextMatrixID(targetToSwap1);
            int previousID2 = GetPreviousMatrixID(targetToSwap2);
            int nextID2 = GetNextMatrixID(targetToSwap2);
            // calculate new duration
            newDuration = workDay.Duration;
            // remove
            // orders are next to eachother: o---1---2---o
            if (targetToSwap2.PreviousOrder != null && targetToSwap1.ID == targetToSwap2.PreviousOrder.ID)
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID1, targetToSwap1.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, targetToSwap2.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, nextID2);

                newDuration += Program.Matrix.GetDriveTimeInMin(previousID1, targetToSwap2.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, targetToSwap1.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, nextID2);
            }
            // orders are next to eachother: o---2---1---o
            else if (targetToSwap1.PreviousOrder != null && targetToSwap2.ID == targetToSwap1.PreviousOrder.ID)
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID2, targetToSwap2.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, targetToSwap1.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, nextID1);

                newDuration += Program.Matrix.GetDriveTimeInMin(previousID2, targetToSwap1.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, targetToSwap2.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, nextID1);
            }
            else
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID1, targetToSwap1.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, nextID1);
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID2, targetToSwap2.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, nextID2);

                newDuration += Program.Matrix.GetDriveTimeInMin(previousID1, targetToSwap2.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap2.MatrixID, nextID1);
                newDuration += Program.Matrix.GetDriveTimeInMin(previousID2, targetToSwap1.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToSwap1.MatrixID, nextID2);
            }
            // add
            if (newDuration > Program.maxWorkMinutes)
            {
                return false;
            }

            score += newDuration - workDay.Duration;
            return true;
        }
        public override void Apply()
        {
            if (next1 != null && next1.ID == targetToSwap2.ID)
            {
                targetToSwap1.Unlink();
                targetToSwap1.Link(ref targetToSwap2, ref next2);
            }
            else if (next2 != null && next2.ID == targetToSwap1.ID)
            {
                targetToSwap2.Unlink();
                targetToSwap2.Link(ref targetToSwap1, ref next1);
            }
            else
            {
                targetToSwap1.Unlink();
                targetToSwap1.Link(ref previous2, ref targetToSwap2);
                targetToSwap2.Unlink();
                targetToSwap2.Link(ref previous1, ref next1);
            }

            workDay.UpdateDuration(newDuration);
        }
    }

    class MoveOrder : Operator
    {
        WorkDay workDay = null;
        Route route = null;
        Order targetToMove = null;
        Order moveToTarget = null;
        Order previous2 = null;
        Order next2 = null;
        bool infront = false;
        float newDuration = 0;
        Truck targetTruck;

        public override bool Try(Truck[] truck, out float score)
        {
            score = 0;
            targetTruck = truck[Program.Random.Next(truck.Length)];
            BitDay day = targetTruck.GetRandomDay();
            workDay = targetTruck.GetWorkDay(day);
            route = workDay.Routes[Program.Random.Next(workDay.Routes.Length)];
            infront = Program.Random.Next(2) == 1;

            if (route.Count <= 1)
            {
                return false;
            }
            // get targets
            targetToMove = route.Peek(out _);
            moveToTarget = route.Peek(out _);
            while (targetToMove.ID == moveToTarget.ID)
                moveToTarget = route.Peek(out _);
            // previous and next
            previous2 = moveToTarget.PreviousOrder;
            next2 = moveToTarget.NextOrder;
            // id
            int previousID1 = GetPreviousMatrixID(targetToMove);
            int nextID1 = GetNextMatrixID(targetToMove);
            int previousID2 = GetPreviousMatrixID(moveToTarget);
            int nextID2 = GetNextMatrixID(moveToTarget);
            // update duration
            newDuration = workDay.Duration;
            // o---1---2---o
            // move it to the right of moveToTarget
            if (previous2 != null && targetToMove.ID == previous2.ID)
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID1, targetToMove.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID1);
                newDuration -= Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, nextID2);

                newDuration += Program.Matrix.GetDriveTimeInMin(previousID1, moveToTarget.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, targetToMove.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID2);
            }
            // o---2---1---o
            // move it to the left of moveToTarget
            else if (next2 != null && next2.ID == targetToMove.ID)
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID2, moveToTarget.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID1, targetToMove.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID1);

                newDuration += Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, nextID1);
                newDuration += Program.Matrix.GetDriveTimeInMin(previousID2, targetToMove.MatrixID);
                newDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, moveToTarget.MatrixID);
            }
            else
            {
                newDuration -= Program.Matrix.GetDriveTimeInMin(previousID1, targetToMove.MatrixID);
                newDuration -= Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID1);
                newDuration += Program.Matrix.GetDriveTimeInMin(previousID1, nextID1);
                if (infront)
                {
                    newDuration -= Program.Matrix.GetDriveTimeInMin(previousID2, moveToTarget.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(previousID2, targetToMove.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, moveToTarget.MatrixID);
                }
                else
                {
                    newDuration -= Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, nextID2);
                    newDuration += Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, targetToMove.MatrixID);
                    newDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID2);
                }
            }

            // check if we were working overtime, but with this op we are under
            if (newDuration > Program.maxWorkMinutes)
            {
                return false;
            }
            score += newDuration - workDay.Duration;
            return true;
        }

        public override void Apply()
        {
            // o---1---2---o
            // move it to the right of moveToTarget
            if (previous2 != null && targetToMove.ID == previous2.ID)
            {
                targetToMove.Unlink();
                targetToMove.Link(ref moveToTarget, ref next2);
            }
            // o---2---1---o
            // move it to the left of moveToTarget
            else if (next2 != null && next2.ID == targetToMove.ID)
            {
                targetToMove.Unlink();
                targetToMove.Link(ref previous2, ref moveToTarget);
            }
            else
            {
                if (infront)
                {
                    targetToMove.Unlink();
                    Order previousOrder = moveToTarget.PreviousOrder;
                    targetToMove.Link(ref previousOrder, ref moveToTarget);
                }
                else
                {
                    targetToMove.Unlink();
                    Order nextOrder = moveToTarget.NextOrder;
                    targetToMove.Link(ref moveToTarget, ref nextOrder);
                }
            }
            workDay.UpdateDuration(newDuration);
        }
    }
    class SwapDay : Operator
    {
        WorkDay currentWorkDay = null;
        Route currentRoute = null;
        WorkDay targetWorkDay = null;
        Route targetRoute = null;
        Order targetToMove = null;
        Order moveToTarget = null;
        BitDay targetDay;
        BitDay currentDay;
        bool infront = false;
        float newCurrentDuration = 0;
        float newTargetDuration = 0;
        float newCurrentWeight = 0;
        float newTargetWeight = 0;
        int targetToMoveIndex = 0;
        Truck targetTruck;
        public override bool Try(Truck[] truck, out float score)
        {
            score = 0;
            targetTruck = truck[Program.Random.Next(truck.Length)];
            currentDay = targetTruck.GetRandomDay();
            currentWorkDay = targetTruck.GetWorkDay(currentDay);
            currentRoute = currentWorkDay.Routes[Program.Random.Next(currentWorkDay.Routes.Length)];
            if (currentRoute.Count == 0)
            {
                return false;
            }
            targetToMove = currentRoute.Peek(out targetToMoveIndex);
            infront = Program.Random.Next(2) == 1;
            targetDay = targetTruck.GetRandomDay();
            while (targetDay == currentDay || (targetToMove.Frequency == 2 && targetDay == BitDay.Wednesday) || (targetToMove.Frequency == 3 && (targetDay == BitDay.Tuesday || targetDay == BitDay.Thursday)))
                targetDay = targetTruck.GetRandomDay();
            targetWorkDay = targetTruck.GetWorkDay(targetDay);
            targetRoute = targetWorkDay.Routes[Program.Random.Next(targetWorkDay.Routes.Length)];

            int previousID = GetPreviousMatrixID(targetToMove);
            int nextID = GetNextMatrixID(targetToMove);
            // update duration
            newCurrentDuration = currentWorkDay.Duration;
            newTargetDuration = targetWorkDay.Duration;
            if (currentRoute.Count == 1)
            {
                newCurrentDuration -= 30; //remove 30 minutes since we will have 1 trip less to base
            }
            if (targetRoute.Count == 0)
            {
                newTargetDuration += 30;
            }
            newCurrentDuration -= Program.Matrix.GetDriveTimeInMin(previousID, targetToMove.MatrixID);
            newCurrentDuration -= Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextID);
            newCurrentDuration += Program.Matrix.GetDriveTimeInMin(previousID, nextID);

            int previousOrderMatrixID = -1;
            int nextOrderMatrixID = -1;
            moveToTarget = targetRoute.Peek(out int index);
            if (moveToTarget != null)
            {
                if (infront)
                {
                    previousOrderMatrixID = GetPreviousMatrixID(moveToTarget);
                }
                else
                {
                    nextOrderMatrixID = GetNextMatrixID(moveToTarget);
                }
            }
            newTargetDuration += targetToMove.EmptyingTime;
            newCurrentDuration -= targetToMove.EmptyingTime;
            if (moveToTarget != null)
            {
                if (infront)
                {
                    newTargetDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, moveToTarget.MatrixID);
                    newTargetDuration += Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, targetToMove.MatrixID);
                    newTargetDuration -= Program.Matrix.GetDriveTimeInMin(previousOrderMatrixID, moveToTarget.MatrixID);
                }
                else
                {
                    newTargetDuration += Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, targetToMove.MatrixID);
                    newTargetDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, nextOrderMatrixID);
                    newTargetDuration -= Program.Matrix.GetDriveTimeInMin(moveToTarget.MatrixID, nextOrderMatrixID);
                }
            }
            else
            {
                newTargetDuration += Program.Matrix.GetDriveTimeInMin(Program.baseMatrixID, targetToMove.MatrixID);
                newTargetDuration += Program.Matrix.GetDriveTimeInMin(targetToMove.MatrixID, Program.baseMatrixID);
            }
            if (newTargetDuration > Program.maxWorkMinutes || newCurrentDuration > Program.maxWorkMinutes)
            {
                return false;
            }

            // weight increase
            newTargetWeight = targetRoute.Weight + (targetToMove.VolumeContainer * targetToMove.NContainers);
            newCurrentWeight = currentRoute.Weight - (targetToMove.VolumeContainer * targetToMove.NContainers);
            if (newTargetWeight > Program.maxCapacity)
            {
                return false;
            }

            // decline penalty
            int frequency = targetToMove.Frequency;
            if (frequency != 1)
            {
                int parentID = targetToMove.OriginalID;
                Order parent = Program.Database.GetOrder(parentID);
                int visits = 0; // set current day because target has no day assigned
                if (parent.ID != targetToMove.ID)
                    visits |= (int)parent.BitDay;
                for (int i = 0; i < parent.Children.Count; i++)
                {
                    Order child = Program.Database.GetOrder(parent.Children[i]);
                    if (child.ID != targetToMove.ID)
                        visits |= (int)child.BitDay;
                }
                int temp = visits;
                if (Truck.CheckFrequency(frequency, temp |= (int)targetDay))
                {
                    score -= parent.EmptyingTime * parent.Frequency * 3;
                }
                if (Truck.CheckFrequency(frequency, visits |= (int)currentDay))
                {
                    score += parent.EmptyingTime * parent.Frequency * 3;
                }
            }
            score += newCurrentDuration - currentWorkDay.Duration;
            score += newTargetDuration - targetWorkDay.Duration;
            return true;

        }
        public override void Apply()
        {
            Order previousRandom = null;
            Order nextRandom = null;
            targetToMove.Unlink();
            if (moveToTarget != null)
            {
                previousRandom = moveToTarget.PreviousOrder;
                nextRandom = moveToTarget.NextOrder;
            }
            if (infront)
            {
                targetToMove.Link(ref previousRandom, ref moveToTarget);
            }
            else
            {
                targetToMove.Link(ref moveToTarget, ref nextRandom);
            }

            targetToMove.SetDay(targetDay);
            currentRoute.UpdateWeight(newCurrentWeight);
            targetRoute.UpdateWeight(newTargetWeight);
            currentWorkDay.UpdateDuration(newCurrentDuration);
            targetWorkDay.UpdateDuration(newTargetDuration);
            currentRoute.Remove(targetToMoveIndex);
            targetRoute.Push(targetToMove.ID);
        }
    }
    class SwapTruck : Operator
    {
        BitDay day;
        Order target1;
        Order target2;
        int index1;
        int index2;
        float newDuration1;
        float newWeight1;
        float newDuration2;
        float newWeight2;
        WorkDay workDay1;
        WorkDay workDay2;
        Route route1;
        Route route2;
        Truck targetTruck1;
        Truck targetTruck2;

        public override bool Try(Truck[] truck, out float score)
        {
            score = 0;
            targetTruck1 = truck[0];
            targetTruck2 = truck[1];
            day = targetTruck1.GetRandomDay();
            Route[] routes = new Route[4];
            routes[0] = targetTruck1.GetWorkDay(day).Routes[0];
            routes[1] = targetTruck1.GetWorkDay(day).Routes[1];
            routes[2] = targetTruck2.GetWorkDay(day).Routes[0];
            routes[3] = targetTruck2.GetWorkDay(day).Routes[1];
            int randomIndex = Program.Random.Next(2);
            route1 = routes[randomIndex];
            int randomIndex2 = Program.Random.Next(2, 4);
            while (randomIndex == randomIndex2)
                randomIndex2 = Program.Random.Next(2, 4);
            route2 = routes[randomIndex2];
            if (randomIndex <= 1)
                workDay1 = targetTruck1.GetWorkDay(day);
            else
                workDay1 = targetTruck2.GetWorkDay(day);
            if (randomIndex2 <= 1)
                workDay2 = targetTruck1.GetWorkDay(day);
            else
                workDay2 = targetTruck2.GetWorkDay(day);

            if (route1.Count < 2 || route2.Count < 2)
            {
                return false;
            }

            target1 = route1.Peek(out index1);
            target2 = route2.Peek(out index2);

            //duration
            int previousMID1 = GetPreviousMatrixID(target1);
            int nextMID1 = GetNextMatrixID(target1);
            int previousMID2 = GetPreviousMatrixID(target2);
            int nextMID2 = GetNextMatrixID(target2);
            newDuration1 = workDay1.Duration;
            newDuration2 = workDay2.Duration;

            newDuration1 -= target1.EmptyingTime;
            newDuration1 += target2.EmptyingTime;
            newDuration1 -= Program.Matrix.GetDriveTimeInMin(previousMID1, target1.MatrixID);
            newDuration1 -= Program.Matrix.GetDriveTimeInMin(target1.MatrixID, nextMID1);
            newDuration1 += Program.Matrix.GetDriveTimeInMin(previousMID1, target2.MatrixID);
            newDuration1 += Program.Matrix.GetDriveTimeInMin(target2.MatrixID, nextMID1);

            if (newDuration1 > Program.maxWorkMinutes)
                return false;

            newDuration2 -= target2.EmptyingTime;
            newDuration2 += target1.EmptyingTime;
            newDuration2 -= Program.Matrix.GetDriveTimeInMin(previousMID2, target2.MatrixID);
            newDuration2 -= Program.Matrix.GetDriveTimeInMin(target2.MatrixID, nextMID2);
            newDuration2 += Program.Matrix.GetDriveTimeInMin(previousMID2, target1.MatrixID);
            newDuration2 += Program.Matrix.GetDriveTimeInMin(target1.MatrixID, nextMID2);

            if (newDuration2 > Program.maxWorkMinutes)
                return false;

            newWeight1 = route1.Weight - (target1.NContainers * target1.VolumeContainer);
            newWeight1 += target2.NContainers * target2.VolumeContainer;
            newWeight2 = route2.Weight - (target2.NContainers * target2.VolumeContainer);
            newWeight2 += target1.NContainers * target1.VolumeContainer;
            if (newWeight1 > Program.maxCapacity || newWeight2 > Program.maxCapacity)
                return false;

            score += newDuration1 - workDay1.Duration;
            score += newDuration2 - workDay2.Duration;
            return true;
        }
        public override void Apply()
        {
            Order p1 = target1.PreviousOrder;
            Order n1 = target1.NextOrder;
            Order p2 = target2.PreviousOrder;

            target1.Unlink();
            target1.Link(ref p2, ref target2);
            target2.Unlink();
            target2.Link(ref p1, ref n1);

            route1.Remove(index1);
            route2.Remove(index2);
            route1.Push(target2.ID);
            route2.Push(target1.ID);

            workDay1.UpdateDuration(newDuration1);
            workDay2.UpdateDuration(newDuration2);

            route1.UpdateWeight(newWeight1);
            route2.UpdateWeight(newWeight2);

        }
    }
}
