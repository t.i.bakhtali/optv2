﻿using System;
using System.IO;

namespace PickUpProblem
{
    [Serializable()]
    class Config
    {
        public Truck[] Trucks;
        public Database Database;
        public NotVisited NotVisited;
        public float Score;
        public float BestScore;
        public static Config Load(string filename)
        {
            Config config = BinarySerialization.ReadFromBinaryFile<Config>(Program.routeFolder + filename + ".config");
            Program.Database = config.Database;
            Program.NotVisited = config.NotVisited;
            return config;
        }
        public static void Save(string filename, Config config, Database database, NotVisited notVisited)
        {
            config.Database = database;
            config.NotVisited = notVisited;
            BinarySerialization.WriteToBinaryFile(Program.routeFolder + filename + ".config", config);
        }
    }

    class SimAnnealing
    {
        int iteration = 0;
        int maxIteration;
        double currTemp = 10;
        const double minTemp = 0.05;
        const double alpha = 0.975;
        const int iterationOfT = 150000;
        public Config config = new Config();
        bool usingSave = false;

        public SimAnnealing(int maxIteration, int truckN)
        {
            this.maxIteration = maxIteration;
            config.Trucks = new Truck[truckN];
            config.Score = Program.maxDeclinePenalty;
            config.BestScore = Program.maxDeclinePenalty;
            for (int i = 0; i < truckN; i++)
            {
                config.Trucks[i] = new Truck();
            }
        }
        public SimAnnealing(int maxIteration, string filename)
        {
            this.maxIteration = maxIteration;
            config = Config.Load(filename);
            Console.WriteLine("score: " + config.Score);
            Console.WriteLine("bestscore: " + config.BestScore);
            usingSave = true;
        }
        public void Save(string filename)
        {
            Config.Save(filename, config, Program.Database, Program.NotVisited);
            Data.ParseToFile(filename, config.Trucks);
        }

        public void Run()
        {
            Console.WriteLine("running simulation...");

            int denied = 0;
            int addIterations = 20000000;
            Operator op = new AddOrder();
            Truck truck = null;

            if (usingSave)
            {
                addIterations = 0;
                currTemp = 0.1;
            }
            else
            {
                // if Data.SeperateHighFreq = false then this will just return 
                InitRoute initRoute = new InitRoute();
                initRoute.Apply(config.Trucks, ref config.Score);
            }

            while (iteration < addIterations)
            {
                if (usingSave)
                {
                    op = Program.Proberator.GetRandomOp();
                    truck = config.Trucks[Program.Random.Next(config.Trucks.Length)];
                }
                if (!op.Try(config.Trucks, out float adjEnergyDiff))
                {
                    iteration++;
                    continue;
                }
                op.Apply();
                config.Score += adjEnergyDiff;
                if (config.Score < config.BestScore)
                {
                    config.BestScore = config.Score;
                }
                iteration++;
            }

            while (iteration < maxIteration && currTemp >= minTemp)
            {
                op = Program.Proberator.GetRandomOp();

                // Check if the operation is feasible
                if (!op.Try(config.Trucks, out float adjEnergyDiff))
                {
                    iteration++;
                    if (iteration % iterationOfT == 0) currTemp *= alpha;
                    continue;
                }
                if (adjEnergyDiff <= 0)
                {
                    op.Apply();
                    config.Score += adjEnergyDiff;
                    if (config.Score < config.BestScore)
                        config.BestScore = config.Score;
                }
                else if (AcceptProb(-adjEnergyDiff, currTemp) >= Program.Random.NextDouble())
                {
                    op.Apply();
                    config.Score += adjEnergyDiff;
                    if (config.Score < config.BestScore)
                        config.BestScore = config.Score;
                }
                else
                {
                    denied++;
                }
                iteration++;
                if (iteration % iterationOfT == 0) currTemp *= alpha;

            }
            Console.WriteLine("...simulation ended");
            Console.WriteLine();
            Console.WriteLine("denied adjacent states: " + denied);
            Console.WriteLine("score: " + config.Score);
            Console.WriteLine("bestscore: " + config.BestScore);
        }

        double AcceptProb(double adjEnergyDiff, double currTemp)
        {
            return Math.Exp(adjEnergyDiff / currTemp);
        }
    }
}
