﻿using System;
using System.Collections.Generic;

namespace PickUpProblem
{
    struct ProbOp
    {
        public Operator Operator { get; private set; }
        public int LeftBound;
        public int RightBound;

        public ProbOp(Operator op, int left, int right)
        {
            Operator = op;
            LeftBound = left;
            RightBound = right;
        }
    }

    class Proberator
    {
        private List<ProbOp> probOps = new List<ProbOp>();

        private int maxProb = 0;

        public void AddOperator(Operator op, int range)
        {
            if(probOps.Count == 0)
            {
                ProbOp probOp = new ProbOp(op, 0, range);
                probOps.Add(probOp);
            }
            else
            {
                int leftBound = probOps[probOps.Count - 1].RightBound;
                maxProb = leftBound + range;
                ProbOp probOp = new ProbOp(op, leftBound, maxProb);
                probOps.Add(probOp);
            }
        }

        public Operator GetRandomOp()
        {
            int prob = Program.Random.Next(maxProb);
            return SearchOp(prob, 0, probOps.Count);
        }

        private Operator SearchOp(int probality, int left, int right)
        {
            if (right >= left)
            {
                int mid = left + (right - left) / 2;
                ProbOp probOp = probOps[mid];
                if (probality >= probOp.LeftBound && probality < probOp.RightBound)
                {
                    return probOp.Operator;
                }
                if (probality < probOp.LeftBound)
                {
                    return SearchOp(probality, left, mid - 1);
                }
                else
                {
                    return SearchOp(probality, mid + 1, right);
                }
            }
            return null;
        }
    }
}
