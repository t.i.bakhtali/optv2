﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace PickUpProblem
{
    enum BitDay { Monday = 1, Tuesday = 2, Wednesday = 4, Thursday = 8, Friday = 16, NA = 0 }
    enum Header { ID, Location, Frequency, NContainers, VolumeContainer, EmptyingTime, MatrixID, X, Y }
    struct Cell
    {
        public Cell(int distance, int driveTime)
        {
            Distance = distance;
            DriveTime = driveTime;
        }

        public int Distance { get; private set; }
        public int DriveTime { get; private set; }
    }

    [Serializable()]
    class Order
    {
        public Order PreviousOrder { get; private set; } = null;
        public Order NextOrder { get; private set; } = null;
        public int ID { get; private set; }
        public int ParentID { get; private set; } = -1;
        public int OriginalID
        {
            get {
                if (ParentID == -1) return ID;
                return ParentID;
            }
        }
        public List<int> Children { get; private set; }
        public string Location { get; private set; }
        public int Frequency { get; private set; }
        public int NContainers { get; private set; }
        public int VolumeContainer { get; private set; }
        public float EmptyingTime { get; private set; }
        public int MatrixID { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public BitDay BitDay { get; private set; } = BitDay.NA;
        //
        #region Getters & Setters
        public void SetPreviousOrder(Order order)
        {
            PreviousOrder = order;
        }
        public void SetNextOrder(Order order)
        {
            NextOrder = order;
        }
        public void SetDay(BitDay day)
        {
            BitDay = day;
        }
        #endregion
        //
        public Order(string[] items)
        {
            Init(items);
            Children = new List<int>();
        }

        public Order(string[] items, int childID, int parentID)
        {
            Init(items);
            this.ID = childID;
            this.ParentID = parentID;
        }
        public void Init(string[] items)
        {
            ID = int.Parse(items[(int)Header.ID]);
            Location = items[(int)Header.Location];
            Frequency = int.Parse(items[(int)Header.Frequency].Substring(0, 1));
            NContainers = int.Parse(items[(int)Header.NContainers]);
            VolumeContainer = int.Parse(items[(int)Header.VolumeContainer]);
            EmptyingTime = float.Parse(items[(int)Header.EmptyingTime], CultureInfo.InvariantCulture.NumberFormat);
            MatrixID = int.Parse(items[(int)Header.MatrixID]);
            X = int.Parse(items[(int)Header.X]);
            Y = int.Parse(items[(int)Header.Y]);
        }

        public void Link(ref Order orderPrev, ref Order orderNext)
        {
            if (orderPrev != null)
            {
                orderPrev.SetNextOrder(this);
                this.SetDay(orderPrev.BitDay);
            }

            this.SetPreviousOrder(orderPrev);
            this.SetNextOrder(orderNext);

            if (orderNext != null)
            {
                orderNext.SetPreviousOrder(this);
                this.SetDay(orderNext.BitDay);
            }
        }

        public void Unlink()
        {
            if (PreviousOrder != null)
            {
                PreviousOrder.SetNextOrder(NextOrder);
            }
            if (NextOrder != null)
            {
                NextOrder.SetPreviousOrder(PreviousOrder);
            }
            this.SetPreviousOrder(null);
            this.SetNextOrder(null);
            this.SetDay(BitDay.NA);
        }

        public void SwapDirection()
        {
            Order temp = NextOrder;
            NextOrder = PreviousOrder;
            PreviousOrder = temp;
        }
    }

    [Serializable()]
    class Database
    {
        Dictionary<int, Order> orders = new Dictionary<int, Order>();

        public void Add(int orderID, Order order)
        {
            orders.Add(orderID, order);
        }

        public bool Contains(int orderID)
        {
            return orders.ContainsKey(orderID);
        }

        public Order GetOrder(int orderID)
        {
            return orders[orderID];
        }
    }

    class Matrix
    {
        private Cell[,] distanceMatrix;
        public Matrix(int size)
        {
            distanceMatrix = new Cell[size, size];
        }

        public void Add(int i, int j, int distance, int driveTime)
        {
            distanceMatrix[i, j] = new Cell(distance, driveTime);
        }

        public float GetDriveTimeInMin(int startID, int destinationID)
        {
            return distanceMatrix[startID, destinationID].DriveTime / 60f;
        }
    }

    [Serializable()]
    class NotVisited
    {
        private List<int> orders = new List<int>();
        public int Count { get => orders.Count; }

        // Get order and remove from list
        public int Pull()
        {
            int index = Program.Random.Next(orders.Count);
            int orderID = orders[index];
            Remove(index);
            return orderID;
        }

        public void Push(int orderID)
        {
            orders.Add(orderID);
        }

        public Order Peek(out int index)
        {
            if (orders.Count == 0)
            {
                index = -1;
                return null;
            }
            index = Program.Random.Next(orders.Count);
            return Program.Database.GetOrder(orders[index]);
        }

        public void Remove(int index)
        {
            orders[index] = orders[orders.Count - 1];
            orders.RemoveAt(orders.Count - 1);
        }
    }

    class Data
    {
        public static bool SeperateHighFreq = false;
        public static List<int> HighFreq = new List<int>();

        public static void ParseOrders(string orderFile, ref Database database, ref NotVisited notVisited)
        {
            StreamReader sreader = File.OpenText(orderFile + ".txt");
            // skip the first line
            string headerFile = sreader.ReadLine();
            string line;
            int childID = Program.largestOrderNr + 1;
            while ((line = sreader.ReadLine()) != null)
            {
                // remove whitespace
                line = string.Concat(line.Where(c => !Char.IsWhiteSpace(c)));
                // split string
                string[] items = line.Split(';');
                // parse order
                Order order = new Order(items);
                // add order to database
                database.Add(order.ID, order);
                // add order to not visited

                if (!SeperateHighFreq) 
                {
                    notVisited.Push(order.ID);

                    if (order.Frequency > 1)
                    {
                        for (int i = 0; i < order.Frequency - 1; i++)
                        {
                            Order childOrder = new Order(items, childID, order.ID);
                            order.Children.Add(childID);
                            database.Add(childID, childOrder);
                            notVisited.Push(childID);
                            childID++;
                        }
                    }
                }
                else
                {
                    if (order.Frequency > 1)
                    {
                        HighFreq.Add(order.ID);
                        for (int i = 0; i < order.Frequency - 1; i++)
                        {
                            Order childOrder = new Order(items, childID, order.ID);
                            order.Children.Add(childID);
                            database.Add(childID, childOrder);
                            childID++;
                        }
                    }
                    else
                    {
                        notVisited.Push(order.ID);
                    }
                }
            }
            sreader.Close();
            Console.WriteLine("> orders imported");
        }

        public static void ParseMatrix(string distanceMatrixFile, ref Matrix matrix)
        {
            StreamReader sreader = File.OpenText(distanceMatrixFile + ".txt");
            // skip the first line
            string headerLine = sreader.ReadLine();
            string line;
            while ((line = sreader.ReadLine()) != null)
            {
                string[] items = line.Split(';');
                matrix.Add(int.Parse(items[0]), int.Parse(items[1]), int.Parse(items[2]), int.Parse(items[3]));
            }
            sreader.Close();
            Console.WriteLine("> distance matrix created");
        }

        public static void ParseToFile(string filename, Truck[] trucks)
        {
            using (StreamWriter file = new StreamWriter(Program.routeFolder + filename + ".txt"))
            {
                for (int t = 0; t < trucks.Length; t++)
                {
                    int truckID = t + 1;
                    foreach (var item in trucks[t].WorkDays)
                    {
                        int day = IntDay(item.Key);
                        int seqN = 1;
                        WorkDay workDay = item.Value;

                        for (int i = 0; i < workDay.Routes.Length; i++)
                        {
                            Route route = workDay.Routes[i];
                            if (route.Count == 0)
                            {
                                continue;
                            }
                            //Order currOrder = FindStart(route.Peek(out int x));
                            Order currOrder = FindStart(route);

                            while (currOrder.NextOrder != null)
                            {
                                file.WriteLine(Format(t, day, ref seqN, currOrder.OriginalID));
                                currOrder = currOrder.NextOrder;
                            }
                            file.WriteLine(Format(t, day, ref seqN, currOrder.OriginalID));
                            file.WriteLine(Format(t, day, ref seqN, 0));
                        }
                    }
                }
            }
            Console.WriteLine("all routes parsed to " + filename + ".txt");
        }

        private static Order FindStart(Route route)
        {
            for (int i = 0; i < route.Count; i++)
            {
                Order order = route.GetOrder(i);
                if (order.PreviousOrder == null)
                    return order;
            }
            return null;
        }

        //private static Order FindStart(Order order)
        //{
        //    if (order.PreviousOrder == null) return order;
        //    return FindStart(order.PreviousOrder);
        //}

        private static string Format(int truckID, int day, ref int seqN, int orderID)
        {
            return (truckID + 1) + ";" + day + ";" + seqN++ + ";" + orderID;
        }
        private static int IntDay(BitDay day)
        {
            if (day == BitDay.Monday) return 1;
            if (day == BitDay.Tuesday) return 2;
            if (day == BitDay.Wednesday) return 3;
            if (day == BitDay.Thursday) return 4;
            if (day == BitDay.Friday) return 5;
            return -1;
        }
    }
}