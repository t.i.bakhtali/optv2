﻿using System;

namespace PickUpProblem
{
    class Program
    {
        public const string dataFolder = "..\\..\\..\\data\\";
        public const string routeFolder = "..\\..\\..\\routes\\";
        // order and matrix fixed data
        public const int orderSize = 1177;
        public const int largestOrderNr = 34910;
        public const int totalFrequency = 1233;
        public const int distanceMatrixSize = 1099;
        // base and truck fixed data
        public const int baseID = 0;
        public const int baseMatrixID = 287;
        public const int dumpTimeMinutes = 30;
        public const int maxCapacity = 100000; // 20000 * 5
        public const int maxWorkMinutes = 720; // 12 hours
        // fixed penalties
        public const float maxDeclinePenalty = 10679.33999f; // minutes
        // static variables
        public static Random Random = new Random();
        public static Database Database = new Database();
        public static Matrix Matrix = new Matrix(distanceMatrixSize);
        public static NotVisited NotVisited = new NotVisited();
        // operators
        public static Proberator Proberator = new Proberator();

        static void AddOperators()
        {
            Proberator.AddOperator(new AddOrder(), 1);
            Proberator.AddOperator(new SwapOrder(), 3);
            Proberator.AddOperator(new MoveOrder(), 3);
            Proberator.AddOperator(new SwapDay(), 8);
            Proberator.AddOperator(new SwapTruck(), 8);
        }
        // annealing
        public const int iterations = 1000000000;
        private static SimAnnealing SimAnnealing;
        private static string filename;

        static void Main(string[] args)
        {
            AddOperators();

            Data.ParseMatrix(dataFolder + "AfstandenMatrix", ref Matrix);

            Console.WriteLine();
            Console.WriteLine("> load route? (y/n): ");
            ConsoleKey key = Console.ReadKey().Key;
            Console.WriteLine();
            if (key == ConsoleKey.N)
            {
                Console.WriteLine("> init route with high freq orders first? (y/n): ");
                key = Console.ReadKey().Key;
                if (key == ConsoleKey.Y)
                    Data.SeperateHighFreq = true;
                Console.WriteLine();
                Data.ParseOrders(dataFolder + "Orderbestand", ref Database, ref NotVisited);
                SimAnnealing = new SimAnnealing(iterations, 2);
            }
            else if (key == ConsoleKey.Y)
            {
                Console.WriteLine("> filename?: ");
                filename = Console.ReadLine();
                SimAnnealing = new SimAnnealing(iterations, filename);
            }
            Console.WriteLine();
            // run annealing
            //=================
            SimAnnealing.Run();
            //=================
            // conclude
            Console.WriteLine();
            Console.WriteLine("> save route? (y/n): ");
            key = Console.ReadKey().Key;
            Console.WriteLine();
            if (key != ConsoleKey.Y) return;

            Console.WriteLine("> filename?: ");
            filename = Console.ReadLine();
            SimAnnealing.Save(filename);
        }
    }
}
