﻿using System;
using System.Collections.Generic;

namespace PickUpProblem
{
    [Serializable()]
    class Route
    {
        private List<int> orders;
        public float Weight { get; private set; }
        public int Count { get => orders.Count; }
        public Route()
        {
            orders = new List<int>();
        }

        public void UpdateWeight(float newWeight)
        {
            Weight = newWeight;
        }
        public void Push(int orderID)
        {
            orders.Add(orderID);
        }

        public Order Peek(out int index)
        {
            if (orders.Count == 0)
            {
                index = -1;
                return null;
            }
            index = Program.Random.Next(orders.Count);
            return Program.Database.GetOrder(orders[index]);
        }

        public void Remove(int index)
        {
            orders[index] = orders[orders.Count - 1];
            orders.RemoveAt(orders.Count - 1);
        }

        public Order GetOrder(int index)
        {
            return Program.Database.GetOrder(orders[index]);
        }
    }
}